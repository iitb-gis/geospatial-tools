# Geospatial tools from IIT Bombay

Assorted tools and data sets licensed under the permissive Apache 2
license.  Create a subdir for each new tool / data set to be added.

## Farmplot analysis

The farmplot-analysis directory contains tools for analyzing farmplots
data made available as polygons in GeoJSON / shapefile format.  Also
contained therein are geo-tagged PDF maps downloaded from Groundwater
Survey and Development Agency (GSDA).  The maps contain cadastral
polygons for a few villages in Maharashtra.
